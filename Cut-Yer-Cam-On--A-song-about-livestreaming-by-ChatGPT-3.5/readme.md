**"Cut Yer Cam On"**

a song about livestreaming
by ChatGPT-3.5


**Verse 1:**

I'm a live-stream queen, got the internet on lock

B C# D# D# D# D# D# D# - C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B 

With my camera on, I can make your heart stop

B C# D# D# D# D# D# D# - C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B 

I'm the one they come to see, they know I bring the heat

B C# D# D# D# D# D# D# - C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B 

So cut yer cam on, let's start this beat

C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B 


**Chorus:**

Cut yer cam on, cut yer cam on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Let's get it poppin', let's get it on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Cut yer cam on, cut yer cam on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Don't be shy, let's get our freak on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B


**Verse 2:**

Got my OBS set up, got my lighting just right

B C# D# D# D# D# D# D# - C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B 

Gonna take you on a ride that you won't forget tonight

B C# D# D# D# D# D# D# - C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B 

I'm the queen of the stream, and you know it's true

B C# D# D# D# D# D# D# - C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B

So cut yer cam on, and let me see you

C# C# C# C# C# C# C# C# - B B B B B B B B - C# C# C# C# C# C# C# C# - B B B B B B B B


**Bridge:**

I know you're nervous, but don't you worry

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

I'll make you feel good, and you'll be in no hurry

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

To turn that camera off, 'cause you'll be hooked

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

On the livestream queen and the way I look

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B


**Chorus:**

Cut yer cam on, cut yer cam on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Let's get it poppin', let's get it on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Cut yer cam on, cut yer cam on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Don't be shy, let's get our freak on

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B


**Outro:**

So if you're ready, come and join the party

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

Cut yer cam on, and let's get naughty

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

The livestream queen is here to play

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B

So cut yer cam on, and let's get this party underway

C# - C# - C# - C# - B - B - B - B - C# - C# - C# - C# - B - B - B - B
